export const SET_USER = 'SET_USER';
export const CLEAN_USER = 'CLEAN_USER'


export function setUser (user){
    return {type:SET_USER, payload:user}
}


export function cleanUser(){
    return{type:CLEAN_USER}
} 