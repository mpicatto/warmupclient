export const GET_CURRENT = 'GET_CURRENT';
export const CLEAN_CURRENT = 'CLEAN_CURRENT';

export function getCurrent (post){
    return {type:GET_CURRENT, payload:post}
}

export function cleanCurrent(){
    return {type:CLEAN_CURRENT}
}