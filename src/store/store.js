import { createStore, applyMiddleware} from "redux";
import rootReducer from '../reducers/index';
import thunk from "redux-thunk";
import {composeWithDevTools} from 'redux-devtools-extension';

//save state in local store
function saveToLocalStorage(state){
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state',serializedState)
  } catch(e){
    console.log(e)
  }
}

//read localStorage data 
function loadFromLocalStorage(){
  try {

    const serializedState = localStorage.getItem('state')
    if (serializedState === null) return undefined
    return JSON.parse(serializedState) 
  } catch(e){
    console.log(e)
  }
}

//data persistence
 const persistedState = loadFromLocalStorage();


const store = createStore(
  rootReducer,
   persistedState,
  composeWithDevTools(applyMiddleware(thunk),
));

//subscribe functios to local store
store.subscribe(()=>saveToLocalStorage(store.getState()))

export default store;