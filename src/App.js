//libraries
import React from 'react';
import {Route} from 'react-router-dom';

//components
import Loader from './components/home/loader';
import Navigation from './components/navBar/index';
import Home from './components/home/index';
import Editor from './components/create/index';
import Login from './components/login/index';
import Register from './components/login/register';
import Delete from './components/create/deletePost'
import Logout from './components/login/logout'
//styles
import './App.css';

function App() {

  return (
    <div className="App">
      <Route path='/' component={Navigation}/>
      <Route exact path='/' component={Loader} />
      <Route exact path='/home' component={Home} />
      <Route path='/read/:id' component={Home}/>
      <Route exact path='/publish' component={Editor}/>
      <Route path='/edit/:id' component={Editor}/>
      <Route exact path='/login' component={Login}/>
      <Route exact path='/register' component={Register}/>
      <Route exact path='/logout' component={Logout}/>
      <Route path='/delete/:id' component={Delete}/>
    </div>
  );
}

export default App;
