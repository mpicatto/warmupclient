//libraries
import React,{useState} from 'react';
import {useHistory} from 'react-router-dom';
import {Link} from 'react-router-dom'
import Axios from 'axios';
import {connect} from 'react-redux';
import {setUser} from '../../actions/user'

const Login = (props)=>{

    const history = useHistory()
    const [credentials,setCredentials]=useState({username:"", password:""});
    

//page and form controlers    

    const credentialsHandler = (e) =>{
        setCredentials({
            ...credentials,
            [e.target.name]:e.target.value
        })
        console.log(credentials)
    };

    const canceLogin = (e) => {
        history.push('/home')
    }

    const loginUser = (e) =>{
        e.preventDefault();
        Axios.post('http://localhost:3001/login',credentials)
        .then(res=>{
          let data={
            id:res.data.id,
            name:res.data.name,
            lastName:res.data.lastName
          }
          props.setUser(data)
          history.push("/home")
        })
        .catch(err=>{
          alert("Usuario o contraseña incorrecta. \nPor favor vuelva a intentar")
        })
      }

    return(
        <div class="container" style={{marginTop:"100px"}}>
            <h2>Login</h2>
            <form>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="title" >EMAIL:</label>
                    <input 
                    type="email" 
                    class="form-control" 
                    id="email" 
                    name="username"  
                    onChange={(e)=>credentialsHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="title" >PASSWORD:</label>
                    <input 
                    type="password" 
                    class="form-control" 
                    id="password" 
                    name="password" 
                    onChange={(e)=>credentialsHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <div style={{width:"100%",display:"flex",flexDirection:"row",justifyContent:"space-around",marginTop:"10px"}}>
                        <button type="button" class="btn btn-danger btn-sm" style={{width:"30%"}} onClick={(e)=>canceLogin(e)}>Cancelar</button>
                        <button type="button" class="btn btn-success btn-sm" style={{width:"30%"}}onClick={(e)=>loginUser(e)}>Login</button>   
                    </div>
                </div>
                <div>
                    <Link to={"/register"}>
                        <span style={{color:'black',marginTop:"10px"}}>
                        ¿No tiene una cuenta? Regístrese aquí
                        </span>
                    </Link>
                </div>
            </form>
        </div>
    )
}

const mapDispatchToProps = dispatch =>{
    return{
        setUser:(data)=>dispatch(setUser(data))
    }
}

export default connect(null,mapDispatchToProps)(Login)
