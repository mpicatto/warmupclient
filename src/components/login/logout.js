//libraries
import React,{useEffect} from "react";
import {useHistory} from 'react-router-dom';
import Axios from 'axios'
import {connect} from 'react-redux';
import {cleanUser} from '../../actions/user';

export function Loader(props) {
   useEffect(()=>{
    logOut()
    
  },[])
  const history = useHistory()

  const logOut = async (e) =>{
  
    props.cleanUser()
    await Axios.get('http://localhost:3001/logout',{withCredentials:true})
    .then( res=>{
      backToStart()
    })
    .catch(err=>{
      alert(err);
    })
    
  }

  const backToStart =() =>{
    history.push("/home")
  }


  return (
    <div></div>
  );
}

const mapStateToProps = state => {		
  return {		
    user: state.user
  }		
}

const mapDispatchToProps = dispatch => {
  return {
    cleanUser:()=>dispatch(cleanUser()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loader);