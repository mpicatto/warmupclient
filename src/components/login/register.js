//libraries
import React,{useState} from 'react';
import {useHistory} from 'react-router-dom';
import Axios from 'axios';
import {connect} from 'react-redux';

const Register = (props)=>{
    const history = useHistory()
    const [credentials,setCredentials]=useState({
        name:"",
        lastName:"",
        email:"",
        password:"",
        password2:""
    });
    

//page and form controlers    

    const credentialsHandler = (e) =>{
        setCredentials({
            ...credentials,
            [e.target.name]:e.target.value
        })
        console.log(credentials)
    };

    const verifyUser = (e) => {
        let data = credentials
        if (!/\S+@\S+\.\S+/.test(data.email)){
            alert("Debe ingresar un mail valido.(Formato: ejemplo@ejemplo.com)" )
            e.preventDefault();
            setCredentials({
                ...credentials,
                email:"",
                password:"",
                password2:""
            })
          return
        }
        if (data.password !== data.password2){
            alert("Las contraseñas ingresadas deben coincidir")
            setCredentials({
                ...credentials,
                password:"",
                password2:""
            })
          return
         }
         if(!/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,}$/.test(data.password)){
            alert("La contraseña debe tener como minimo 8 caracteres y al menos 1 mayuscula, 1 minúscula y 1 dígito.")
            e.preventDefault();
            setCredentials({
                ...credentials,
                password:"",
                password2:""
            })
              return
          } 
          if (data.password === data.password2 
            && /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,}$/.test(data.password)
            && /\S+@\S+\.\S+/.test(data.email)){
                Axios.post("http://localhost:3001/users",data)
                .then(res=>{
                  console.log(res.data)
                  if(res.data==='ok'){
                      alert("Usuario creado.")
                      history.push("/login")
                    }else{alert("El Email ya esta en uso: utilize uno alternativo")}  
                })
                .catch(err=>{
                    alert('Error de Servidor')
                })
            }
    }

    const cancelRegistration = (e)=>{
        history.push('/login')
    }

    return(
        <div class="container" style={{marginTop:"100px"}}>
            <h2>NUEVO USUARIO</h2>
            <form>
                 <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="title" >NOMBRE:</label>
                    <input 
                    type="text" 
                    class="form-control" 
                    id="name" 
                    name="name"
                    value={credentials.name} 
                    onChange={(e)=>credentialsHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="title" >APELLIDO</label>
                    <input 
                    type="text" 
                    class="form-control" 
                    id="lastName" 
                    name="lastName"
                    value={credentials.lastName}  
                    onChange={(e)=>credentialsHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="email" >EMAIL:</label>
                    <input 
                    type="email" 
                    class="form-control" 
                    id="email" 
                    name="email"
                    value={credentials.email}
                    onChange={(e)=>credentialsHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="password" >PASSWORD:</label>
                    <input
                    type="password" 
                    class="form-control" 
                    id="password" 
                    name="password"
                    value={credentials.password} 
                    onChange={(e)=>credentialsHandler(e)}/>
                    <small id="passHelp" class="form-text text-muted">La contraseña debe tener como minimo 8 caracteres y al menos 1 mayuscula, 1 minúscula y 1 dígito.</small>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="password2" >CONFIRMAR PASSWORD:</label>
                    <input 
                    type="password"
                    class="form-control"
                    id="password2" 
                    name="password2"
                    value={credentials.password2} 
                    onChange={(e)=>credentialsHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <div style={{width:"100%",display:"flex",flexDirection:"row",justifyContent:"space-around",marginTop:"10px"}}>
                        <button type="button" class="btn btn-danger btn-sm" style={{width:"30%"}} onClick={(e)=>cancelRegistration(e)}>Cancelar</button>
                        <button type="button" class="btn btn-success btn-sm" style={{width:"30%"}} onClick={(e)=>verifyUser(e)}>Registrarse</button>   
                    </div>
                </div>
            </form>
        </div>
    )
}

const mapDispatchToProps = dispatch =>{
    return{
    }
}

export default connect(null,mapDispatchToProps)(Register)
