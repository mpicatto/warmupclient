//libraries
import React,{useState} from 'react';

//components
import Navigation from '../navBar/index';
import Login from './login';


const LoginMain = (props) => {

    const [toogle,setToogle]=useState(true)

    return(
        <div style={{ background:"url(/img/background.png) no-repeat center center fixed ",backgroundColor:"white"}}>
            <Navigation />
            {toogle?<Login />:null}
     
        </div>
    )
}

export default LoginMain