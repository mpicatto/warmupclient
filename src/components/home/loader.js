//libraries
import React,{useEffect} from "react";
import {useHistory} from 'react-router-dom';
import Axios from 'axios'
import {connect} from 'react-redux';
import {getPosts} from '../../actions/posts'
import {getCategories} from '../../actions/categories'
import {cleanCurrent} from '../../actions/currentPost'


export function Loader(props) {
   useEffect(()=>{
    getPosts(props)
  },[])
  const history = useHistory()

    const getPosts = async(props)=>{
     await Axios.get(`http://localhost:3001/posts`)
     .then(res=>{
       console.log(res.data)
      props.getCategories(res.data.categoryList)
      if (res.data.posts.length>0){
        props.getPosts(res.data.posts)
      }else{
        props.getPosts([])
      }
      
      history.push("/home")
     })
     .catch(err=>{
      alert('Error de Servidor')
  })

  }

  return (
<div></div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    getPosts:(posts)=>dispatch(getPosts(posts)),
    getCategories:(categories)=>dispatch(getCategories(categories)),
    cleanCurrent:()=>dispatch(cleanCurrent())

  }
}

export default connect(null, mapDispatchToProps)(Loader);