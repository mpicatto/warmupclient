//libraries
import React from 'react';
import{useParams} from 'react-router-dom';

//components
import Posts from './postList';
import Reader from './readPost';

const Home = (props) => {

    const postId = useParams();

    return(
        <div style={{ background:"url(/img/background.png) no-repeat center center fixed ",backgroundColor:"white"}}>
            {!postId.id?<Posts />:<Reader id={postId}/>}         
        </div>
    )
}

export default Home