//libraries
import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import Axios from 'axios';

const PostList = (props) => {



    return(
        <div id="posts">
            <div className="container" style={{marginTop:"100px"}}>
                <hr style={{backgroundColor:"#6372ff",width:"100%"}}/>
                {props.postsList.map(post=>{
                    return(
                        <div>
                            <div style={{display:'flex',justifyContent:"flex-start",marginLeft:"1%"}}>
                                <h6>{post.published}</h6>
                            </div>
                            <div >
                                <h3><Link to={`/read/${post.postId}`} >"{post.title}"</Link></h3>
                                <h5>{`por:${post.user.name} ${post.user.lastName}`}</h5>
                            </div>
                            {props.user.id===post.userId?
                            <div style={{width:"100%",display:"flex",flexDirection:"row",justifyContent:"space-around",marginTop:"10px"}}>
                                <button 
                                type="button" 
                                class="btn btn-danger btn-sm" 
                                style={{width:"30%"}}>
                                    <Link to={`/delete/${post.postId}`} style={{textEmphasisColor:"white"}}>
                                        Eliminar
                                    </Link></button>
                                <button 
                                type="button" 
                                class="btn btn-default btn-sm" 
                                style={{width:"30%"}}>
                                    <Link to={`/edit/${post.postId}`} 
                                    style={{textEmphasisColor:"white"}}>
                                        Editar
                                    </Link>
                                </button>   
                            </div>:null}
                            <hr style={{backgroundColor:"#6372ff",width:"100%"}}/>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

const mapStateToProps = state =>{
    return{
        postsList:state.posts.posts,
        user:state.user
    }
}

export default connect(mapStateToProps) (PostList)