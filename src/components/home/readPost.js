//libraries
import React,{useEffect} from 'react';
import {connect} from 'react-redux';
import {getCurrent} from '../../actions/currentPost'
import Axios from 'axios';

const ReadPost = (props) => {

    useEffect(()=>{
        getBody(props)
    },[])

    const getBody = (props)=>{
        Axios.get(`http://localhost:3001/posts/${props.id.id}`)
        .then(res=>{
          console.log(res.data)
          props.getCurrent(res.data)
        })
        .catch(err=>{
            alert('Error de Servidor')
        })
    }

    return(
        <div id="reader">
            <div className="container" style={{marginTop:"100px"}}>
                <hr style={{backgroundColor:"#6372ff",width:"100%"}}/>
                <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around'}}>
                <h6>Fecha de Publicación: {props.currentPost.published} </h6>
                <h6>Categoría: {props.currentPost.category.name}</h6>
                </div>
                <div>
                    <img src={props.currentPost.headerImg?props.currentPost.headerImg:"/img/defaultHeader.png"} style={{width:"70%", marginBottom:"30px",borderRadius:"15px"}}/>
                </div>
                <div>
                    <h2>{props.currentPost.title}</h2>
                    <h3>por: Autor</h3>
                </div>
                <div>
                    <p>{props.currentPost.body}</p>
                </div>
                 <hr style={{backgroundColor:"#6372ff",width:"100%"}}/>
            </div>
        </div>
    )
}

const mapStateToProps = state =>{
    return{
        postsList:state.posts.posts,
        currentPost:state.currentPost
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        getCurrent:(post)=>dispatch(getCurrent(post))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ReadPost)