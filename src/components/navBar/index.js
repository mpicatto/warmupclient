//libraries
import React from 'react';
import {connect} from 'react-redux';

const Navigation = (props) =>{
  console.log(props.user.id)

    return(
        <nav id="menu" className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1"
            >
              {" "}
              <span className="sr-only">Toggle navigation</span>{" "}
              <span className="icon-bar"></span>{" "}
              <span className="icon-bar"></span>{" "}
              <span className="icon-bar"></span>{" "}
            </button>
            <a className="navbar-brand page-scroll" href="#page-top">
              ALKEMY techblog
            </a>{" "}
          </div>

          <div
            className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1"
          >
            <ul className="nav navbar-nav navbar-right">

              <li>
                <a href="/home" className="page-scroll">
                  HOME
                </a>
              </li>

              <li>
                {props.user.id===0?null:<a href="/publish" className="page-scroll">
                  PUBLICAR
                </a>}
              </li>
              <li>                           
                {props.user.id===0?<a href="/login"  className="page-scroll">
                  LOGIN
                </a>:<a href="/logout"  className="page-scroll">
                  LOGOUT
                </a>}
              </li>
            </ul>
          </div>
        </div>
     </nav>
    )
}

const mapStateToProps = state =>{
  return{
    user:state.user
  }
}
export default connect(mapStateToProps)(Navigation)