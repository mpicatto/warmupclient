//libraries
import React,{useEffect,useState} from 'react';
import {useHistory} from 'react-router-dom';
import Axios from 'axios';
import {connect} from 'react-redux';
import {getCurrent,cleanCurrent} from '../../actions/currentPost'

const Publisher = (props)=>{
    useEffect(()=>{
        setTitle(props);
        getBody(props);
    },[])
    const history = useHistory()
    const [pageTitle,setPageTitle]=useState("Publicar Nuevo Post");
    const [postData,setPostData]=useState({
        categoryId:'',
        userId:props.userId,
        title: '',
        body: '',
        headerImg: '',
        published: '',
    })

//page and form controlers    
    const setTitle = (props)=> {
        if (props.task==='create'){
            setPageTitle('Publicar Nuevo Post')
        }else{
            setPageTitle('Editar Post')
        }
    };

    const getBody = async (props)=>{
        console.log(props.task)
        if (props.task==='edit'){
        Axios.get(`http://localhost:3001/posts/${props.id.id}`)
        .then(res=>{
          props.getCurrent(res.data);
        })
        .catch(err=>{
            alert('Error de Servidor');
        })
        setPostData({
            ...postData,
            categoryId:props.currentPost.categoryId,
            title:props.currentPost.title,
            body:props.currentPost.body,
            headerImg:props.currentPost.headerImg,
            published:props.currentPost.published,
        })
        console.log(postData)
        }
        return
    };

    const dataHandler = (e) =>{
        setPostData({
            ...postData,
            [e.target.name]:e.target.value
        })
        console.log(postData)
    };

    const cancelPublication = (e)=>{
        history.push('/home')
    }

    const savePost = (e)=>{
        Axios.post(`http://localhost:3001/posts`,postData)
        .then(res=>{
            if(res.status===201){
                alert("Post publicado")
                history.push("/")
            }
        })
        .catch(err=>{
            alert('Error de Servidor')
        })

    }

    const updatePost = (e)=>{
        props.getCurrent(postData)
        Axios.put(`http://localhost:3001/posts/${props.id.id}`,postData)
        .then(res=>{
            if(res.status===200){
                alert("Post publicado");              
                history.push("/");
            }
        })
        .catch(err=>{
            alert('Error de Servidor')
        })

    }

    return(
        <div class="container" style={{marginTop:"100px"}}>
            <h2>{pageTitle}</h2>
            <form>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="title" >Título:</label>
                    <input 
                    type="text" 
                    class="form-control" 
                    id="title"
                    name="title" 
                    value={postData.title} 
                    onChange={(e)=>dataHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="text" >Texto:</label>
                    <textarea 
                    class="form-control" 
                    id="body"
                    name="body" 
                    rows="10" 
                    value={postData.body} 
                    onChange={(e)=>dataHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="title" >URL imagen:</label>
                    <input 
                    type="text" 
                    class="form-control" 
                    id="headerImg"
                    name="headerImg" 
                    value={postData.headerImg} 
                    onChange={(e)=>dataHandler(e)}/>
                </div>
                <div class="form-group" style={{width:"80%",marginLeft:"10%",textAlign:"left"}}>
                    <label class="control-label" for="sel1" >Categoria:</label>
                    <div style={{width:"100%",display:"flex",flexDirection:"row",justifyContent:"space-around",marginTop:"10px"}}>
                        <select 
                        class="form-control" 
                        id="categoryId"
                        name="categoryId"
                        dir="rtl" style={{width:"30%"}}
                        onChange={(e)=>dataHandler(e)}
                        >
                            <option>elija una opcion</option>
                            {props.categories.map(item=>{
                                return <option value={item.id}>{item.name}</option>
                            })}
                        </select>
                        <button 
                        type="button" 
                        class="btn btn-danger btn-sm" 
                        style={{width:"30%"}} 
                        onClick={(e)=>cancelPublication(e)}>
                            Cancelar
                        </button>
                        <button 
                        type="button" 
                        class="btn btn-success btn-sm" 
                        style={{width:"30%"}}
                        onClick={props.task==='create'?(e)=>savePost(e):(e)=>updatePost(e)}
                        >Publicar</button>   
                    </div>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = state =>{
    return{
        postsList:state.posts.posts,
        currentPost:state.currentPost,
        categories:state.categories.categories,
        userId:state.user.id
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        getCurrent:(post)=>dispatch(getCurrent(post)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Publisher)

