//libraries
import React from 'react';
import{useParams} from 'react-router-dom';

//components
import Editor from './editor'

const Publisher = (props) => {

    const postId = useParams();

    return(
        <div style={{ background:"url(/img/background.png) no-repeat center center fixed ",backgroundColor:"white"}}>
            {!postId.id?<Editor task={'create'}/>:<Editor id={postId} task={'edit'}/>}          
        </div>
    )
}

export default Publisher