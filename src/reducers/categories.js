import {GET_CATEGORIES} from '../actions/categories'


const initialState ={
    categories:[]
}

export default function user (state = initialState, action){

    if (action.type === GET_CATEGORIES){
        return {
            ...state,
            categories:action.payload
        }
    }

    return state
}