import {SET_USER,CLEAN_USER} from '../actions/user'


const initialState ={
    id:0,
    name:"",
    lastName:"",
}

export default function user (state = initialState, action){

    if (action.type === SET_USER){
        return {
            ...state,
            id:action.payload.id,
            name:action.payload.name,
            lastName:action.payload.lastName,
        }
    }

    if (action.type === CLEAN_USER){
        return state = initialState
    }
    return state
}