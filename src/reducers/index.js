import {combineReducers} from 'redux';
import posts from './posts';
import currentPost from './currentPost';
import user from './user';
import categories from './categories';

export default combineReducers({
  posts,currentPost,user,categories
}) 