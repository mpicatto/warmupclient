import {GET_POSTS} from '../actions/posts'


const initialState ={
    posts:[]
}

export default function user (state = initialState, action){

    if (action.type === GET_POSTS){
        return {
            ...state,
            posts:action.payload          
        }
    }

    return state
}