import {GET_CURRENT,CLEAN_CURRENT} from '../actions/currentPost'


const initialState ={
    userID:0,
    author:'',
    id:'',
    categoryId:'',
    category:'',
    headerImg:'',
    title:'',
    body:'',
    published:''
}

export default function user (state = initialState, action){

    if (action.type === GET_CURRENT){
        return {
            ...state,
            userID:action.payload.userID,
            author:action.payload.author,
            id:action.payload.id,
            categoryId:action.payload.categoryId,
            category:action.payload.category,
            headerImg:action.payload.headerImg,
            title:action.payload.title,
            body:action.payload.body,
            published:action.payload.published        
        }
    }    if (action.type === CLEAN_CURRENT){
        return state = initialState    
    }

    return state
}